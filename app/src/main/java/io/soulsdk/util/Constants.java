package io.soulsdk.util;

/**
 * Created by Buiarov on 24/03/16.
 */
public class Constants {

    public static final String ANON_LOGIN_SOURCE = "ANON_LOGIN_SOURCE";
    public static final String PHONE_LOGIN_SOURCE = "PHONE_LOGIN_SOURCE";
    public static final String PASS_LOGIN_SOURCE = "PASS_LOGIN_SOURCE";
    public static final String FACEBOOK_LOGIN_SOURCE = "FACEBOOK_LOGIN_SOURCE";
    public static final String GOOGLE_PLUS_LOGIN_SOURCE = "GOOGLE_PLUS_LOGIN_SOURCE";
}
