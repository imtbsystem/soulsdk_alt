package io.soulsdk.network.managers;

import io.soulsdk.model.general.SoulError;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.requests.PhoneRequestREQ;
import io.soulsdk.model.requests.PhoneVerifyLoginREQ;
import io.soulsdk.util.storage.SoulStorage;

import static io.soulsdk.util.storage.SoulStorage.API_KEY;
import static io.soulsdk.util.storage.SoulStorage.PHONE_NUMBER;
import static io.soulsdk.util.storage.SoulStorage.SESSION_TOKEN;
import static io.soulsdk.util.storage.SoulStorage.USER_ID;
import static io.soulsdk.util.storage.SoulStorage.VERIFICATION_CODE;
import static io.soulsdk.util.storage.SoulStorage.SUCCESSFUL_LOGIN_SOURCE;

/**
 * Created by Buiarov on 06/03/16.
 */

public class AuthCredentials implements Credentials {


    public SoulResponse<PhoneVerifyLoginREQ> getPhoneVerifyREQ(String verificationCode) {
        String apiKey = SoulStorage.getString(API_KEY);
        String phoneNumber = SoulStorage.getString(PHONE_NUMBER);
        if (!apiKey.isEmpty() & !phoneNumber.isEmpty() & !verificationCode.isEmpty()) {
            SoulStorage.save(VERIFICATION_CODE, verificationCode);
            return new SoulResponse<>(new PhoneVerifyLoginREQ(apiKey, verificationCode, phoneNumber));
        } else {
            return new SoulResponse<>(
                    new SoulError("There aren't authorization credentials",
                            SoulError.PHONE_NO_AUTH_CREDENTIALS));
        }
    }

    public SoulResponse<PhoneVerifyLoginREQ> getPhoneVerifyREQ() {
        String apiKey = SoulStorage.getString(API_KEY);
        String phoneNumber = SoulStorage.getString(PHONE_NUMBER);
        String code = SoulStorage.getString(VERIFICATION_CODE);
        String sessionToken = SoulStorage.getString(SESSION_TOKEN);
        if (!apiKey.isEmpty() &
                !phoneNumber.isEmpty() &
                !code.isEmpty() &
                !sessionToken.isEmpty())
            return new SoulResponse<>(new PhoneVerifyLoginREQ(apiKey, code, phoneNumber, sessionToken));
        else
            return new SoulResponse<>(
                    new SoulError("There aren't authorization credentials",
                            SoulError.PHONE_NO_AUTH_CREDENTIALS));
    }

    public void saveSessionToken(String sessionToken) {
        SoulStorage.save(SESSION_TOKEN, sessionToken);
    }

    public SoulResponse<PhoneRequestREQ> getPhoneRequestREQ(String phoneNumber) {
        String apiKey = SoulStorage.getString(API_KEY);
        String method = "sms";
        if (apiKey == null || apiKey.isEmpty()) {
            return new SoulResponse<>(
                    new SoulError("There is no API key",
                            SoulError.PHONE_NO_AUTH_CREDENTIALS));
        } else if (phoneNumber == null || phoneNumber.isEmpty()) {
            return new SoulResponse<>(
                    new SoulError("There is no phone number",
                            SoulError.PHONE_WRONG_NUMBER));
        } else {
            SoulStorage.save(PHONE_NUMBER, phoneNumber);
            return new SoulResponse<>(new PhoneRequestREQ(apiKey, method, phoneNumber));
        }
    }

    public void clearCredentials() {
        SoulStorage.save(SESSION_TOKEN, "");
        SoulStorage.save(PHONE_NUMBER, "");
        SoulStorage.save(VERIFICATION_CODE, "");
        SoulStorage.save(USER_ID, "");
        SoulStorage.save(SUCCESSFUL_LOGIN_SOURCE, "");
        SoulStorage.saveCurrentUser(null);
    }

    public String getUserId() {
        return SoulStorage.getString(USER_ID);
    }

    public String getSessionToken() {
        return SoulStorage.getString(SESSION_TOKEN);
    }

    public void saveUserId(String userId) {
        SoulStorage.save(USER_ID, userId);
    }

    public String getLastSuccessfulLoginSource() {
        return SoulStorage.getString(SoulStorage.SUCCESSFUL_LOGIN_SOURCE);
    }

    public void saveLastSuccessfulLoginSource(String authSource) {
        SoulStorage.save(SoulStorage.SUCCESSFUL_LOGIN_SOURCE, authSource);
    }

    public String getTempLogin() {
        return SoulStorage.getString(SoulStorage.TEMP_LOGIN);
    }

    public String getTempPass() {
        return SoulStorage.getString(SoulStorage.TEMP_PASS);
    }

}
