package io.soulsdk.model.general;

/**
 * General Soul Response
 *
 * @author kitttn
 * @version 0.15
 * @since 28/03/16
 */
public class SoulResponse<T> {
    private SoulError error;
    private boolean hasError = false;
    private T response;

    public SoulError getError() {
        return error;
    }

    public boolean isErrorHappened() {
        return hasError;
    }

    public T getResponse() {
        return response;
    }

    public SoulResponse(T response) {
        this.response = response;
    }

    public SoulResponse(SoulError error) {
        this.error = error;
        this.hasError = true;
    }

    /**
     * Used to indicate that last request completed successfully. It doesn't return a value
     * neither an error.
     */
    public SoulResponse() {
    }


    @Override
    public String toString() {
        return "{error: " + error + "; response: " + response + "}";
    }
}
