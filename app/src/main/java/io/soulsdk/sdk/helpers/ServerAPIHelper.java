package io.soulsdk.sdk.helpers;

import com.google.gson.Gson;

import java.io.File;

import io.soulsdk.model.dto.Album;
import io.soulsdk.model.general.GeneralResponse;
import io.soulsdk.model.general.SoulCallback;
import io.soulsdk.model.general.SoulError;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.requests.AddReceiptREQ;
import io.soulsdk.model.requests.CreateNewAlbumREQ;
import io.soulsdk.model.requests.PassLoginRegisterREQ;
import io.soulsdk.model.requests.PatchAlbumREQ;
import io.soulsdk.model.requests.PatchPhotoREQ;
import io.soulsdk.model.requests.PatchUserREQ;
import io.soulsdk.model.requests.PhoneRequestREQ;
import io.soulsdk.model.requests.PhoneVerifyLoginREQ;
import io.soulsdk.model.requests.ReactionREQ;
import io.soulsdk.model.requests.ReportUserREQ;
import io.soulsdk.model.responses.AlbumRESP;
import io.soulsdk.model.responses.AlbumsRESP;
import io.soulsdk.model.responses.AuthorizationResponse;
import io.soulsdk.model.responses.ChatRESP;
import io.soulsdk.model.responses.ChatsRESP;
import io.soulsdk.model.responses.CurrentUserRESP;
import io.soulsdk.model.responses.EventsRESP;
import io.soulsdk.model.responses.PhoneRequestRESP;
import io.soulsdk.model.responses.PhotoRESP;
import io.soulsdk.model.responses.SubscriptionsAvailableRESP;
import io.soulsdk.model.responses.UserCustomSearchRESP;
import io.soulsdk.model.responses.UserRESP;
import io.soulsdk.model.responses.UsersSearchRESP;
import io.soulsdk.network.adapter.NetworkAdapter;
import io.soulsdk.network.executor.RequestExecutor;
import io.soulsdk.network.managers.Credentials;
import io.soulsdk.network.managers.SearchResultManager;
import io.soulsdk.util.Constants;
import io.soulsdk.util.storage.SoulStorage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

import static io.soulsdk.util.storage.SoulStorage.API_KEY;

/**
 * Created by Buiarov on 09/03/16.
 */
public class ServerAPIHelper {

    private static final String EMPTY_BODY = "";

    public NetworkAdapter adapter;
    public Credentials credentials;

    public ServerAPIHelper(NetworkAdapter adapter, Credentials credentials) {
        this.credentials = credentials;
        this.adapter = adapter;
    }


    public Observable<SoulResponse<PhoneRequestRESP>>
    requestPhone(String phoneNumber, SoulCallback<PhoneRequestRESP> soulCallback) {
        SoulResponse<PhoneRequestREQ> phoneRequest = credentials.getPhoneRequestREQ(phoneNumber);
        if (phoneRequest.isErrorHappened()) {
            if (soulCallback != null) {
                soulCallback.onError(phoneRequest.getError());
                return null;
            } else {
                return Observable.just(new SoulResponse<PhoneRequestRESP>(phoneRequest.getError()));
            }
        } else {
            return new RequestExecutor<>(
                    adapter.getService().requestPhone(phoneRequest.getResponse()),
                    credentials, soulCallback, false)
                    .asObservable();
        }
    }

    public Observable<SoulResponse<AuthorizationResponse>>
    verifyPhone(String verificationCode, SoulCallback<AuthorizationResponse> soulCallback) {
        SoulResponse<PhoneVerifyLoginREQ> phoneVerify = credentials.getPhoneVerifyREQ(verificationCode);
        if (phoneVerify.isErrorHappened()) {
            if (soulCallback != null) {
                soulCallback.onError(phoneVerify.getError());
                return null;
            } else {
                return Observable.just(new SoulResponse<AuthorizationResponse>(phoneVerify.getError()));
            }
        } else {
            return new RequestExecutor<>(
                    adapter.getService()
                            .verifyPhone(phoneVerify.getResponse()),
                    credentials, soulCallback, Constants.PHONE_LOGIN_SOURCE)
                    .asObservable();
        }
    }

    public Observable<SoulResponse<AuthorizationResponse>>
    login(SoulCallback<AuthorizationResponse> soulCallback) {
        switch (credentials.getLastSuccessfulLoginSource()) {
            case Constants.PHONE_LOGIN_SOURCE:
                return loginViaPhone(soulCallback);
            case Constants.PASS_LOGIN_SOURCE:
                return loginWithPass(credentials.getTempLogin(), credentials.getTempPass(), soulCallback);
            default:
                if (soulCallback != null) {
                    soulCallback.onError(new SoulError("No login credentials",
                            SoulError.NO_LOGIN_CREDENTIALS));
                    return null;
                } else {
                    return Observable.just(new SoulResponse<AuthorizationResponse>(new SoulError("No login credentials",
                            SoulError.NO_LOGIN_CREDENTIALS)));
                }
        }
    }

    public Observable<SoulResponse<AuthorizationResponse>>
    loginViaPhone(SoulCallback<AuthorizationResponse> soulCallback) {
        SoulResponse<PhoneVerifyLoginREQ> phoneVerify = credentials.getPhoneVerifyREQ();
        if (phoneVerify.isErrorHappened()) {
            return Observable.just(new SoulResponse<AuthorizationResponse>(phoneVerify.getError()));
        } else {
            return new RequestExecutor<>(
                    adapter.getService()
                            .loginViaPhone(phoneVerify.getResponse()),
                    credentials, soulCallback, false)
                    .asObservable();
        }
    }

    public Observable<SoulResponse<AuthorizationResponse>>
    registerWithPass(String login, String pass, SoulCallback<AuthorizationResponse> soulCallback) {
        return new RequestExecutor<>(
                adapter.getService()
                        .passRegister(new PassLoginRegisterREQ(
                                login, pass, SoulStorage.getString(API_KEY))),
                credentials, soulCallback, Constants.PASS_LOGIN_SOURCE, login, pass)
                .asObservable();
    }

    public Observable<SoulResponse<AuthorizationResponse>>
    loginWithPass(String login, String pass, SoulCallback<AuthorizationResponse> soulCallback) {
        return new RequestExecutor<>(
                adapter.getService()
                        .passLogin(new PassLoginRegisterREQ(
                                login, pass, SoulStorage.getString(API_KEY))),
                credentials, soulCallback, Constants.PASS_LOGIN_SOURCE, login, pass)
                .asObservable();
    }

    public Observable<SoulResponse<GeneralResponse>>
    logout(boolean full, SoulCallback<GeneralResponse> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .logout(full),
                credentials, soulCallback, false, true)
                .asObservable();
    }

    public Observable<SoulResponse<CurrentUserRESP>>
    getMe(SoulCallback<CurrentUserRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getMe(),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<CurrentUserRESP>>
    patchMe(PatchUserREQ patchUserREQ, SoulCallback<CurrentUserRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(patchUserREQ))
                        .patchMe(patchUserREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<SubscriptionsAvailableRESP>>
    getMySubscriptionAvailable(SoulCallback<SubscriptionsAvailableRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getMySubscriptionsAvailable(),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<AlbumsRESP>>
    getMyAlbums(Integer offset, Integer limit, SoulCallback<AlbumsRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getMyAlbums(offset, limit),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<Album>>
    createNewAlbum(CreateNewAlbumREQ createNewAlbumREQ, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(createNewAlbumREQ))
                        .createNewAlbum(createNewAlbumREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<AlbumRESP>>
    getPhotosFromMyAlbum(String albumName, Integer offset, Integer limit, SoulCallback<AlbumRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getPhotosFromMyAlbum(albumName, offset, limit),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<PhotoRESP>>
    addPhotoToMyAlbum(String albumName, File photo, SoulCallback<PhotoRESP> soulCallback) {

        RequestBody description =
                RequestBody.create(MediaType.parse("image/png"), photo);
        MultipartBody.Part file =
                MultipartBody.Part.createFormData("photo", "photo", description);

        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .addPhotoToMyAlbum(albumName, file),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<PatchAlbumREQ>>
    patchAlbum(String albumName, PatchAlbumREQ patchAlbumREQ, SoulCallback<PatchAlbumREQ> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(patchAlbumREQ))
                        .patchAlbum(albumName, patchAlbumREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<Boolean>>
    deleteAlbum(String albumName, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .deleteAlbum(albumName),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<PhotoRESP>>
    getMyPhoto(String albumName, String photoId, SoulCallback<PhotoRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getMyPhoto(albumName, photoId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<PhotoRESP>>
    getUsersPhoto(String albumName, String photoId, SoulCallback<PhotoRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getUsersPhoto(albumName, photoId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<PhotoRESP>>
    patchMyPhoto(String albumName, String photoId, PatchPhotoREQ patchPhotoREQ, SoulCallback<PhotoRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(patchPhotoREQ))
                        .patchMyPhoto(albumName, photoId, patchPhotoREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<PhotoRESP>>
    setPhotoAsMain(String albumName, String photoId, SoulCallback<PhotoRESP> soulCallback) {
        PatchPhotoREQ req = new PatchPhotoREQ();
        req.setMain(true);
        return patchMyPhoto(albumName, photoId, req, soulCallback);
    }

    public Observable<SoulResponse<Boolean>>
    deleteMyPhoto(String albumName, String photoId, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .deleteMyPhoto(albumName, photoId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<UsersSearchRESP>>
    getRecommendations(SearchResultManager.SearchCredentials searchCredentials, SoulCallback<UsersSearchRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getRecommendationsSearchResult(
                                searchCredentials.pageToken,
                                searchCredentials.sessionToken),
                credentials, soulCallback, true)
                .asObservable();
    }

    public Observable<SoulResponse<UserCustomSearchRESP>>
    getNextUsersCustomSearchResult(String filterName, Integer after, Integer limit, SoulCallback<UserCustomSearchRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getNextUsersCustomSearchResult(filterName, after, limit),
                credentials, soulCallback, true)
                .asObservable();
    }

    public Observable<SoulResponse<UserRESP>>
    getUser(String userId, SoulCallback<UserRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getUser(userId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<UserRESP>>
    sendReactionToUser(String userId, String reactingType, ReactionREQ reactionREQ, SoulCallback<UserRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(reactionREQ))
                        .sendReactionToUser(userId, reactingType, reactionREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<GeneralResponse>>
    deleteReactionToUser(String userId, String reactingType, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .deleteReactionToUser(userId, reactingType),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<Boolean>>
    flagUser(String userId, ReportUserREQ reportUserREQ, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(reportUserREQ))
                        .flagUser(userId, reportUserREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<Boolean>>
    deleteFlagUser(String userId, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .deleteFlagUser(userId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<AlbumsRESP>>
    getUsersAlbums(String userId, Integer offset, Integer limit, SoulCallback<AlbumsRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getUsersAlbums(userId, offset, limit),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<AlbumRESP>>
    getUsersPhotosFromAlbum(String userId, String albumName, Integer offset, Integer limit, SoulCallback<AlbumRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getUsersPhotosFromAlbum(userId, albumName, offset, limit),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<ChatsRESP>>
    getAllChats(Integer offset, Integer limit, Boolean showExpired, SoulCallback<ChatsRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getAllChats(offset, limit, showExpired),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<ChatRESP>>
    getOneChat(String chatId, SoulCallback<ChatRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getOneChat(chatId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<Boolean>>
    deleteChat(String chatId, SoulCallback<Boolean> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .deleteChat(chatId),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<EventsRESP>>
    getEvents(Long since, Integer after, Integer limit, SoulCallback<EventsRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        EMPTY_BODY)
                        .getEvents(since, after, limit),
                credentials, soulCallback, false)
                .asObservable();
    }

    public Observable<SoulResponse<CurrentUserRESP>>
    addReceipt(AddReceiptREQ addReceiptREQ, SoulCallback<CurrentUserRESP> soulCallback) {
        return new RequestExecutor<>(
                adapter.getSecuredService(
                        credentials.getUserId(),
                        credentials.getSessionToken(),
                        new Gson().toJson(addReceiptREQ))
                        .addReceipt(addReceiptREQ),
                credentials, soulCallback, false)
                .asObservable();
    }

    public NetworkAdapter getAdapter() {
        return adapter;
    }
}
