package io.soulsdk.sdk;

import android.location.Location;

import java.io.File;

import io.soulsdk.model.dto.Chat;
import io.soulsdk.model.dto.ChatMessage;
import io.soulsdk.model.dto.UsersLocation;
import io.soulsdk.model.general.SoulCallback;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.responses.ChatRESP;
import io.soulsdk.model.responses.ChatsRESP;
import io.soulsdk.sdk.helpers.ServerAPIHelper;
import io.soulsdk.sdk.helpers.chat.ChatsHelper;
import io.soulsdk.util.storage.SoulStorage;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static io.soulsdk.util.storage.SoulStorage.PUBLISH_KEY;
import static io.soulsdk.util.storage.SoulStorage.SUBSCRIBE_KEY;

/**
 * Provides a list of methods for users communication.
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */

public class SoulCommunication {

    private static ServerAPIHelper helper;
    private static ChatsHelper chats;


    private static ChatsHelper getChatsHelper() {
        if (chats == null) {
            String userId = SoulCurrentUser.getCurrentUser().getId();
            chats = new ChatsHelper(userId,
                    SoulCurrentUser.getGCMToken(),
                    SoulStorage.getString(PUBLISH_KEY),
                    SoulStorage.getString(SUBSCRIBE_KEY));
        }
        return chats;
    }

    private SoulCommunication() {
    }

    static void initialize(ServerAPIHelper hp) {
        helper = hp;
    }

    /**
     * Provides all chats of the current authorized User. The method allows to get results by "pages" using
     * limit and offset. There is a {@link io.soulsdk.model.dto.Meta} in server response {@link ChatsRESP}
     * that will give the information to help requesting pages.
     *
     * @param offset       specifies how many results to skip for the first returned result
     * @param limit        specifies how many results to return (default: 20; min: 1; maximum: 100))
     * @param showExpired  if value is 'false' only active chats will be gotten with expiration time more than current server time.
     * @param soulCallback general {@link SoulCallback} of the {@link ChatsRESP}
     */
    public static void getAll(Integer offset, Integer limit, Boolean showExpired, SoulCallback<ChatsRESP> soulCallback) {
        helper.getAllChats(offset, limit, showExpired, soulCallback);
    }

    /**
     * Provides all chats of the current authorized User. The method allows to get results by "pages" using
     * limit and offset. There is a {@link io.soulsdk.model.dto.Meta} in server response {@link ChatsRESP}
     * that will give the information to help requesting pages.
     *
     * @param offset      specifies how many results to skip for the first returned result
     * @param limit       specifies how many results to return (default: 20; min: 1; maximum: 100))
     * @param showExpired if value is 'false' only active chats will be gotten with expiration time more than current server time.
     * @return {@link SoulResponse} of the {@link ChatsRESP} as observable.
     * It is necessary to call subscribe method of observable to get any result.
     */
    public static Observable<SoulResponse<ChatsRESP>> getAll(Integer offset, Integer limit, Boolean showExpired) {
        return helper.getAllChats(offset, limit, showExpired, null);
    }

    /**
     * Provides details of mentioned chat.
     *
     * @param chatId       of the chat
     * @param soulCallback general {@link SoulCallback} of the {@link ChatRESP}
     */
    public static void getOne(String chatId, SoulCallback<ChatRESP> soulCallback) {
        helper.getOneChat(chatId, soulCallback);
    }

    /**
     * Provides details of mentioned chat.
     *
     * @param chatId id of the chat
     * @return {@link SoulResponse} of the {@link ChatRESP} as observable.
     * It is necessary to call subscribe method of observable to get any result.
     */
    public static Observable<SoulResponse<ChatRESP>> getOne(String chatId) {
        return helper.getOneChat(chatId, null);
    }

    /**
     * Deletes mentioned chat.
     *
     * @param chatId       of the chat
     * @param soulCallback general {@link SoulCallback} of the {@link Boolean}
     */
    public static void delete(String chatId, SoulCallback<Boolean> soulCallback) {
        helper.deleteChat(chatId, soulCallback);
    }

    /**
     * Deletes mentioned chat.
     *
     * @param chatId id of the chat
     * @return {@link SoulResponse} of the {@link Boolean} as observable.
     * It is necessary to call subscribe method of observable to get any result.
     */
    public static Observable<SoulResponse<Boolean>> delete(String chatId) {
        return helper.deleteChat(chatId, null);
    }

    /**
     * Loads message history, connects to chat and subscribes to chat, returning messages received.
     * <p><b>Important:</b> this method will also return status messages, like "Sent",
     * "Read", "Delivered" etc.
     * </p>
     *
     * @param chat         the chat to connect and get history
     * @param soulCallback {@link SoulCallback} of the new / unread {@link ChatMessage}s
     */
    public static void connectToChat(Chat chat, SoulCallback<ChatMessage> soulCallback) {
        getChatsHelper().connectAndListen(chat.getId(), chat.getPartnerId(), soulCallback).subscribe();
    }

    /**
     * Loads message history, connects to chat and subscribes to chat, returning messages received.
     * <p><b>Important:</b> this method will also return status messages, like "Sent",
     * "Read", "Delivered" etc.
     * </p>
     *
     * @param chat the chat to connect and get history
     * @return {@link SoulResponse} of the new / unread {@link ChatMessage}s as Observable.
     * <p>It is necessary to call subscribe method of observable to get any result.</p>
     */
    public static Observable<SoulResponse<ChatMessage>> connectToChat(Chat chat) {
        return getChatsHelper().connectAndListen(chat.getId(), chat.getPartnerId(), null)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    /**
     * Sends message from current user to chat. Sending message with this method ensures that message
     * will be delivered, even if errors happen.
     *
     * @param chatId  id of the chat
     * @param message text of the message to send
     */
    public static void sendChatMessage(String chatId, String message) {
        getChatsHelper().sendMessage(chatId, message);
    }

    /**
     * Uploads file to chat album, and returns a message with photo, which you've sent to chat
     *
     * @param chatId id of the chat to send photo
     * @param file   Image file to send.
     */
    public static void sendChatPhoto(String chatId, File file) {
        getChatsHelper().sendMessage(chatId, file);
    }

    /**
     * Sends your location to chat
     *
     * @param chatId id of the chat you want to share location
     */
    public static void sendChatCurrentLocation(String chatId) {
        UsersLocation location = SoulCurrentUser.getLastLocation();
        getChatsHelper().sendMessage(chatId, location);
    }

    /**
     * Sends your location to chat
     *
     * @param chatId id of the chat you want to share location
     */
    public static void sendChatLocation(String chatId, Location location) {
        UsersLocation loc = new UsersLocation(location.getLatitude(), location.getLongitude());
        getChatsHelper().sendMessage(chatId, loc);
    }

    /**
     * Sends a service notification in chat that message ​<b>deliveredMessage</b>​ was delivered to you.
     * Should be called when user receives the message.
     *
     * @param deliveredMessage {@link ChatMessage} you want to mark as delivered
     */
    public static void markMessageDelivered(ChatMessage deliveredMessage) {

    }

    /**
     * Sends a service notification in chat that message <b>readMessage</b> was read by you.
     * Should be called when user reads the message.
     *
     * @param readMessage {@link ChatMessage} you want to mark as read
     */
    public static void markMessageRead(ChatMessage readMessage) {

    }

    /**
     * Unsubscribe current user from future messages retrieval on selected chat
     *
     * @param chatId id of the chat you want to unsubscribe
     */
    public static void disconnect(String chatId) {
        getChatsHelper().unsubscribe(chatId);
    }
}