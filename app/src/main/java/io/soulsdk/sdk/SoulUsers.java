package io.soulsdk.sdk;

import io.soulsdk.model.general.SoulCallback;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.responses.UserCustomSearchRESP;
import io.soulsdk.model.responses.UserRESP;
import io.soulsdk.model.responses.UsersSearchRESP;
import io.soulsdk.network.managers.SearchResultManager;
import io.soulsdk.sdk.helpers.ServerAPIHelper;
import rx.Observable;

/**
 * Provides a collection of methods for retrieving users using different filters.
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */
public class SoulUsers {

    private static ServerAPIHelper helper;

    private SoulUsers() {
    }

    static void initialize(ServerAPIHelper hp) {
        helper = hp;
    }

    /**
     * <p>
     * Returns users sorted by rules defined in your Application Configuration Page in back-end
     * administration console. By default method always returns users that was not seen before next
     * {@link #refreshSearchResult()} or was not ever reacted, or its reactions are expired.
     * By default only users that has availableTill parameter set to unixtime endpoint somewhere in
     * the future will be returned by this method.
     * </p>
     * (but this behaviour can be changed in the Application Configuration Page in back-end
     * administration console)
     *
     * @param soulCallback general {@link SoulCallback} of {@link UsersSearchRESP}
     */
    public static void getNextSearchResult(SoulCallback<UsersSearchRESP> soulCallback) {
        helper.getRecommendations(SearchResultManager.getNextUsersSearchResult(), soulCallback);
    }

    /**
     * <p>
     * Returns users sorted by rules defined in your Application Configuration Page in back-end
     * administration console. By default method always returns users that was not seen before next
     * {@link #refreshSearchResult()} or was not ever reacted, or its reactions are expired.
     * By default only users that has availableTill parameter set to unixtime endpoint somewhere in
     * the future will be returned by this method.
     * </p>
     * (but this behaviour can be changed in the Application Configuration Page in back-end
     * administration console)
     *
     * @return observable of general {@link SoulResponse} of {@link UsersSearchRESP}
     */
    public static Observable<SoulResponse<UsersSearchRESP>> getNextSearchResult() {
        return helper.getRecommendations(SearchResultManager.getNextUsersSearchResult(), null);
    }

    /**
     * <p>
     * After this method was called server clear cashed results and method
     * {@link #getNextSearchResult(SoulCallback)} will return new search result with users was got
     * before refreshSearchResult and new users added later with a fresh sorting.
     * </p>
     */
    public static void refreshSearchResult() {
        SearchResultManager.refreshSearchResult();
    }

    /**
     * Returns a pageable list of users filtered by a predefined filter {filterName}
     *
     * @param filterName   string value as defined in your Application Configuration Page in
     *                     back-end administration console. Note that you cannot user
     *                     "recommendations" as a filter name, because it's reserved.
     * @param after        if set, returns only items with recordId larger than after
     * @param limit        specifies how many results to return (default: 20; min: 1; maximum: 100))
     * @param soulCallback general {@link SoulCallback} of {@link UserCustomSearchRESP}
     */
    public static void getNextCustomSearchResult(String filterName, Integer after, Integer limit, SoulCallback<UserCustomSearchRESP> soulCallback) {
        helper.getNextUsersCustomSearchResult(filterName, after, limit, soulCallback);
    }

    /**
     * Returns a pageable list of users filtered by a predefined filter {filterName}
     *
     * @param filterName string value as defined in your Application Configuration Page in
     *                   back-end administration console. Note that you cannot user
     *                   "recommendations" as a filter name, because it's reserved.
     * @param after      if set, returns only items with recordId larger than after
     * @param limit      specifies how many results to return (default: 20; min: 1; maximum: 100))
     * @return observable of general {@link SoulResponse} of {@link UserCustomSearchRESP}
     */
    public static Observable<SoulResponse<UserCustomSearchRESP>>
    getNextCustomSearchResult(String filterName, Integer after, Integer limit) {
        return helper.getNextUsersCustomSearchResult(filterName, after, limit, null);
    }

    /**
     * Returns single user by specified userId. <b>Note:</b> it is possible to access only the user
     * you've already loaded using {@link #getNextSearchResult(SoulCallback)} or
     * {@link #getNextCustomSearchResult(String, Integer, Integer, SoulCallback)} after the last
     * {@link #refreshSearchResult()} method calling.
     *
     * @param userId       id of specified user
     * @param soulCallback general {@link SoulCallback} of {@link UserRESP}
     */
    public static void getUser(String userId, SoulCallback<UserRESP> soulCallback) {
        helper.getUser(userId, soulCallback);
    }

    /**
     * Returns single user by specified userId. <b>Note:</b> it is possible to access only the user
     * you've already loaded using {@link #getNextSearchResult()} or
     * {@link #getNextCustomSearchResult(String, Integer, Integer)} after the last
     * {@link #refreshSearchResult()} method calling.
     *
     * @param userId id of specified user
     * @return observable of general {@link SoulResponse} of {@link UserRESP}
     */
    public static Observable<SoulResponse<UserRESP>> getUser(String userId) {
        return helper.getUser(userId, null);
    }

}
